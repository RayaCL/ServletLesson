package cn.servlet.lesson4;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class HttpSessionDataChange implements HttpSessionAttributeListener {

	public void attributeAdded(HttpSessionBindingEvent se) {
		// TODO Auto-generated method stub
		String key=se.getName();
		String value=se.getValue().toString();
		System.out.println(key+"---"+value);
	}

	public void attributeRemoved(HttpSessionBindingEvent se) {
		// TODO Auto-generated method stub

	}

	public void attributeReplaced(HttpSessionBindingEvent se) {
		// TODO Auto-generated method stub
		String key=se.getName();
		String value=se.getValue().toString();
		String val=se.getSession().getAttribute(key).toString();
		System.out.println("replace--"+key+"--"+value+"--"+val);
		
	}

}
