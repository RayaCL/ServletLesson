package cn.servlet.lesson4;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFilter implements Filter {
	/*
	 * 生命周期销毁方法
	 */
	public void destroy() {
		// TODO Auto-generated method stub

	}
	/*
	 * 配置了拦截某些资源 这些资源都别进入doFilter
	 * 方法决定了资源是否能够通过该过滤器
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		//调用该方法
		HttpServletRequest req=(HttpServletRequest)request;
		HttpServletResponse resp=(HttpServletResponse)response;
		
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html;charset=UTF-8");
		
		String url=req.getRequestURI();
		System.out.println(url);
		//如果访问了file就通过访问了private
		if(!url.contains("private")){
			chain.doFilter(request, response);
		}
		

	}
	/*
	 * 生命周期的初始化方法对方的
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
