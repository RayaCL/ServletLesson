package cn.servlet.lesson4;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
/*
 * 监听request对象的产生和销毁
 * request 的生命周期 浏览器发起请求 产生request对象 响应后request别销毁
 */
public class RequestCycleListener implements ServletRequestListener {

	public void requestDestroyed(ServletRequestEvent sre) {
		// TODO Auto-generated method stub
		System.out.println("请求销毁");
	}

	public void requestInitialized(ServletRequestEvent sre) {
		// TODO Auto-generated method stub
		HttpServletRequest req=(HttpServletRequest)sre.getServletRequest();
		System.out.println("请求产生了"+req.getRequestURL());
	}

}
