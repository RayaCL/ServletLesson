package cn.servlet.lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContextServlet extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		//全局对象
		ServletContext sc=this.getServletContext();
		//1.servlet之间共享数据
		sc.setAttribute("sex", "男");
		//2.获取web.xml中配置的参数
		String driverClass=sc.getInitParameter("driverClass");
		out.println("获取的参数:"+driverClass);
		//3.读取web资源
		InputStream is=sc.getResourceAsStream("/my.txt");
		BufferedReader br=new BufferedReader(new InputStreamReader(is,"GBK"));
		String str=br.readLine();
		out.println("获取资源内容:"+str);
		br.close();
		is.close();
		//打印日志
		sc.log("执行完毕");
		//使用config对象获取servlet的自身配置参数
		String s=config.getInitParameter("hello");
		out.println("自身配置参数:"+s);
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		super.doPost(request, response);
	}
	private ServletConfig config;
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		this.config=config;
		super.init(config);
	}
}
