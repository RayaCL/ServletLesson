package cn.servlet.lesson5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class MyShareDownload extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	static String SAVE_DIR="F:/myfile";
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<form  method=\"post\""+
                        "enctype=\"multipart/form-data\">"+
                        "<input type='text' name='userName' /><br/>"+
                        "<input type=\"file\" name=\"myFile\" size=\"50\" /><br />"+
                        "<input type=\"submit\" value=\"上传文件\" />"+
				"</form>");
		//接受文件
		boolean isMultipart=ServletFileUpload.isMultipartContent(request);
		//判读是否是上传文件请求
		if(isMultipart){
			//创建解析文件上传的工厂类
			DiskFileItemFactory factory=new DiskFileItemFactory();
			//从请求中解析出文件
			ServletFileUpload upload=new ServletFileUpload(factory);
			upload.setHeaderEncoding("UTF-8");
			try {
				//解析出文件的个数
				List<FileItem> items=upload.parseRequest(request);
				for(FileItem f:items){
					//输入的文本框
					if(f.isFormField()){
						System.out.println(f.getFieldName());
					}else{
						String fileName=f.getName();
						InputStream is=f.getInputStream();
						String destPath=SAVE_DIR+"/"+fileName;
						FileOutputStream fis=new FileOutputStream(destPath);
						byte[] bt=new byte[1024];
						int readN=-1;
						while((readN=is.read(bt))!=-1){
							fis.write(bt,0,readN);
						}
						fis.flush();
						fis.close();
					}
				}
			} catch (FileUploadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		File file=new File(SAVE_DIR);
		File[] files=file.listFiles();
		for(File f:files){
			out.println("<a href='DownloadServlet?fileName="+f.getName()+"'>"+f.getName()+"<br/>");
		}
		out.flush();
		out.close();
	}
	
}
