package cn.servlet.lesson5;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UploadServlet extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	static String SAVE_DIR="F:/myfile";
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		//接收文件
		boolean isMultipart=ServletFileUpload.isMultipartContent(request);
		//判断是否是文件上传请求
		if(isMultipart){
			//用于创建解析文件上传的工厂类
			DiskFileItemFactory factory=new DiskFileItemFactory();
			//默认上传的文件 暂时存放在临时目录 System.getProperty("java.io.tmpdir")
			//也有可能是tomcat的temp文件中
			//用于从请求中解析出文件
			ServletFileUpload upload=new ServletFileUpload(factory);
			upload.setHeaderEncoding("UTF-8");
			try {
				//解析出文件的个数
				List<FileItem> items=upload.parseRequest(request);
				System.out.println(items.size());
				for(FileItem f:items){
					//表示输入的文本框  isFormFileoad是true
					if(f.isFormField()){
						System.out.println(f.getFieldName()+">="+f.getString());
					}else{
						String fileName=f.getName();
						InputStream is=f.getInputStream();
						String destPath=SAVE_DIR+"/"+fileName;
						FileOutputStream fis=new FileOutputStream(destPath);
						byte[] bt=new byte[1024];
						int readN=-1;
						while((readN=is.read(bt))!=-1){
							fis.write(bt,0,readN);
						}
					}
				}
			} catch (FileUploadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//重定向
		response.sendRedirect("http://localhost:8080/ServletLesson/MyShareDownload");
	}

}
