package cn.servlet.lesson1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddUser {
	public static void addUser(User u){
		Connection conn;
		try {
			conn = DbUtils.getConnection();
			String sql="insert into myuser(username,password) values(?,?)";
			PreparedStatement sta=conn.prepareStatement(sql);
			sta.setString(1, u.getUsername());
			sta.setString(2, u.getPassword());
			sta.execute();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
