package cn.servlet.lesson1;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbUtils {
	static Properties p=new Properties();
	static{
		InputStream is=DbUtils.class.getResourceAsStream("jdbcmysql.properties");
		try {
			p.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		String url=p.getProperty("url");
		String driverClass=p.getProperty("driverClass");
		String uName=p.getProperty("userName");
		String password=p.getProperty("password");
		Class.forName(driverClass);
		Connection conn=DriverManager.getConnection(url, uName, password);
		return conn;
	}
}
