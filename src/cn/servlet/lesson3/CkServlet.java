package cn.servlet.lesson3;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CkServlet extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	int i=0;
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		/*//无状态型
//		out.println("访问的次数:"+(++i));
		//有状态 将变量写入到浏览器互相不影响
		Cookie[] ck=request.getCookies();
		int i=1;
		//如果某个浏览器是第一次访问没有cookie 第一次访问需要给cookie添加初始值1
		if(ck==null||ck.length==0){
			Cookie c=new Cookie("i",i+"");
			response.addCookie(c);
		}else{
			for(Cookie co:ck){
				//判断cookie中是否存在i这个cookie如果存在加1
				if(co.getName().equals("i")){
					i=Integer.parseInt(co.getValue())+1;
				}
			}
			Cookie c=new Cookie("i",i+"");
			response.addCookie(c);
			
		}*/
		//有状态通过session来实现
		HttpSession session=request.getSession();
		System.out.println(session.getId());
		//获取session中i的值
		Object obj=session.getAttribute("i");
		int i=1;
		//第一次i为1
		if(obj==null){
			session.setAttribute("i", i);
		}else{
			String value=request.getParameter("ck");
			if(value==null){
				i=(Integer)obj+i;
				session.setAttribute("i", i);
			}else if(value.equals("aa")){
				i=(Integer)obj-1;
				session.setAttribute("i", i);
				if(i<=0){
					response.sendRedirect("http://localhost:8080/ServletLesson/CkServlet");
				}
			}
		}
		out.println("<a href='http://localhost:8080/ServletLesson/CkServlet?ck=aa'>来点啊</a>");
		out.println("第"+i+"次");
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		this.doGet(request, response);
		out.flush();
		out.close();
	}       

}
