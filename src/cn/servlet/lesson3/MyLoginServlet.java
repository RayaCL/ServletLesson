package cn.servlet.lesson3;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

import cn.servlet.lesson1.DbUtils;


public class MyLoginServlet extends HttpServlet {
	/*
	 * 请求转发和重定向的区别
	 * 1.调用方式不同:转发request.getRequestDispatcher("/lesson02/detail.html").forward(request, response);
	 * 				重定向:response.sendRedirect(request.getContextPath()+"/lesson02/detail.html");
	 * 2.请求次数不同:转发一次重定向2次
	 * 3.跳转方式不同:转发服务器内部跳转浏览器不知道 重定向告知浏览器302设置location发生的二次跳转
	 * 4.地址不同:转发地址不变,重定向变化为第二次地址
	 * 5.参数不同:转发保留参数重定向参数丢失
	 * 6.跨越不同:转发只能在同一个项目中的url跳转重定向可以跨越
	 */

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		String userName=request.getParameter("userName");
		String password=request.getParameter("password");
			String sql="Select * from myuser where username=? and password=?";
			PreparedStatement sta;
			try {
				sta = conn.prepareStatement(sql);
				sta.setString(1, userName);
				sta.setString(2, password);
				ResultSet rs=sta.executeQuery();
				if(rs.next()){
//					out.print("<font>登录成功</font>");
					//请求转发
					//跳转到detail.html
//					request.getRequestDispatcher("/lesson02/detail.html").forward(request, response);
					//重定向
					response.sendRedirect(request.getContextPath()+"/lesson02/detail.html");
					return;
				}else{
					out.print("<font>登录失败</font>");
					//页面重新刷新(跳转)
					response.setHeader("refresh", "5;url="+request.getContextPath()+"/index.html");
				}
				rs.close();
				sta.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public Connection conn;
	public void init() throws ServletException {
		try {
			conn=DbUtils.getConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void destroy() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.destroy();
	}

}
